package com.java110.intf.common;

import com.java110.dto.msg.MsgReadDto;

import java.util.List;

/**
 * @ClassName IMsgReadInnerServiceSMO
 * @Description 消息阅读接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/

public interface IMsgReadInnerServiceSMO {

    /**
     * <p>查询小区楼信息</p>
     *
     * @param msgReadDto 数据对象分享
     * @return MsgReadDto 对象数据
     */
    List<MsgReadDto> queryMsgReads(MsgReadDto msgReadDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param msgReadDto 数据对象分享
     * @return 小区下的小区楼记录数
     */

    int queryMsgReadsCount(MsgReadDto msgReadDto);
}
