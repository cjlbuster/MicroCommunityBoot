package com.java110.intf.code;

import javax.servlet.http.HttpServletRequest;

/**
 * @ClassName ICodeApi
 * @Description 编码生成服务
 * @Author wuxw
 * @Date 2019/4/23 9:09
 * @Version 1.0
 * add by wuxw 2019/4/23
 **/

public interface ICodeApi {

    /**
     * 生成编码
     *
     * @param orderInfo 订单信息
     * @param request   request对象
     * @return 编码对象
     */

    String generatePost(String orderInfo, HttpServletRequest request);

    /**
     * 生成 编码
     *
     * @param prefix 前缀
     * @return 编码
     */
    String generateCode(String prefix);
}
